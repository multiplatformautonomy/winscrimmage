# Debugging
This file serves to document debugging attempts.

**Unexpected Behavior During predator_prey_boids.xml mission - ACTIVE**\
Issue:\
*After capturing a couple enemies, the next capture seems to not register and the predator ends up
in a spiral with one of the boids. Additionally, the boids do not behave identically to those when
running scrimmage natively. When the boids are run with a motion model besides JSBSim, however,
they behave as expected.*\
How to reproduce?\
*Run the predator_prey_boids.xml mission.*

Partial Fix:\
*Preventing the SimpleCapture plugin from rechecking distance fixes part of this problem. This
eliminates the scenario where an enemy can be placed in the capture list without being removed from
the field.*

Notes:\
*With this fix, it became clear that the boids also do not behave as expected. These errors seem to
suggest high message latency, but upon inspection the latency is almost identical to what is seen
from Ubuntu scrimmage. Additionally, the boids only behave strangely when using the JSBSim motion
model. Without a predator in the mission, the boids with lower fidelity motion models perform
identically to those in Ubuntu scrimmage. With the predator, the output is extremely similar but
final positions are off by 0.1 or 0.2 units.*

**Scrimmage takes a while to load, and seems to hang a bit after the window closes - ACTIVE**\
Issue:\
*The time between the window opening and the visuals being loaded is much longer 
than on Ubuntu.  In addition, if the mission runs to completion the program seems to hang
for a while, instead of returning immediately like on Ubuntu. This occurs with or without
the GUI enabled.*\
How to reproduce?\
*Run a mission with or without the GUI enabled.*

**Protobuf 3.3.0 causes strange errors during Scrimmage compilation - ACTIVE**\
Issue:\
*Building scrimmage with protobuf 3.3.0 causes the build to fail and complain that the protobuf
function "GetMessage" is pure virtual. This can be worked around by modifying the protobuf header
file, but I'm not sure how safe the fix is.*\
How to reproduce:\
*Attempt to build scrimmage using MinGW's gcc with protobuf 3.3.0.*

**Protobuf failing to parse log files - SOLVED**\
Issue:\
*The log files can't be parsed. They seem to be failing at scrimmage_proto::\*::\_internalParse.*
*This indicates either missing required fields, or the message is corrupted.*\
How to reproduce?\
*using scrimmage-playback will always cause this issue.*

Fix:\
*add the 'O_BINARY' to every 'open' call.*

Explanation:
- posix 'open' on windows needs the 'O_BINARY' flag to prevent new-line conversion.
- New-line conversion also occurs when the file is being read.
	- The log files are read through a stream that is processed internally by protobuf, hiding the fact that new-line conversion is occuring.
