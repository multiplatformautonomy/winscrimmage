#!/bin/bash
#This script builds and installs boost 1.58 with the libraries that scrimmage requires.

#error catching
set -e
trap 'echo "buildBoost.sh failed!"' ERR

#checking to see if we are doing a debug build - buildScrimmage.sh should have handled any bad arguments.
if [[ $1 == "debug" ]]; then
	build_type="debug"
else
	build_type="release"
fi

#downloading boost if we haven't already
mkdir -p deps
cd deps

if [ ! -d boost_1_58_0 ]; then
	wget https://sourceforge.net/projects/boost/files/boost/1.58.0/boost_1_58_0.tar.gz
	echo "extracting..."
	tar xfpz boost_1_58_0.tar.gz
	rm boost_1_58_0.tar.gz
fi

cd boost_1_58_0

#building required libraries and installing to /usr/local/
./bootstrap.bat gcc
./b2 -j 8 toolset=gcc variant=$build_type threading=multi \
				                --prefix="/usr/local" \
						--with-thread \
						--with-date_time \
						--with-iostreams \
						--with-program_options \
						--with-regex \
						--with-filesystem \
						--with-system \
						--with-graph \
						--with-atomic \
						install
