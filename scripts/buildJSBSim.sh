#!/bin/bash
#This script builds and installs JSBSim.

#error catching
set -e
trap 'echo "buildJSBSim.sh failed!"' ERR

mkdir -p deps
cd deps

#checking to see if we are doing a debug build - buildScrimmage.sh should have handled any bad arguments.
if [[ $1 == "debug" ]]; then
	build_type="Debug"
else
	build_type="Release"
fi

#cloning the JSBSim git repo and checking out the required commit
if [ ! -d jsbsim ]; then 
	git clone https://github.com/JSBSim-Team/jsbsim.git
fi

cd jsbsim
git checkout 4840a621dafe1ce64970990ddaec3e86a1c7e082

#apply jsbsim.patch and a winscrimmage patch
third_party_dir="../../scrimmage/3rd-party"
$third_party_dir/setup/apply-patch.sh $third_party_dir/patches/jsbsim.patch
$third_party_dir/setup/apply-patch.sh ../../patches/jsbsim_01_prevent_using_feenableexcept.patch

#update with install-aircraft.sh
$third_party_dir/setup/install-aircraft.sh .

#building
mkdir -p build
cd build
cmake .. -G "MSYS Makefiles" \
         -DCMAKE_BUILD_TYPE=$build_type \
		 -DBUILD_SHARED_LIBS=ON \
		 -DINSTALL_JSBSIM_EXEC=ON \
		 -DCMAKE_INSTALL_PREFIX=/usr/local

make -j8

#installing
make install
