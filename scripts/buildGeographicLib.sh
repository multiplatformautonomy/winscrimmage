#!/bin/bash
#Build and install GeographicLib.sh

#error catching
set -e
trap 'echo "buildGeographicLib.sh failed!"' ERR

#checking to see if we are doing a debug build - buildScrimmage.sh should have handled any bad arguments.
if [[ $1 == "debug" ]]; then
	build_type="Debug"
else
	build_type="Release"
fi

mkdir -p deps
cd deps 

#downloading the GeographicLib source if we don't already have it
if [ ! -d ./GeographicLib-1.50.1 ]; then 
	wget -A.tar.gz https://sourceforge.net/projects/geographiclib/files/distrib/GeographicLib-1.50.1.tar.gz

	#unzipping and cleaning up a bit
	echo "Unzipping..."
	tar xfpz GeographicLib-1.50.1.tar.gz
	rm GeographicLib-1.50.1.tar.gz
else
	echo "existing GeographicLib folder found!"
fi

#building
cd GeographicLib-1.50.1

mkdir -p build
cd build
cmake .. -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=$build_type -DCMAKE_INSTALL_PREFIX="/usr/local"
make -j8

#testing
make test

#installing
make install
