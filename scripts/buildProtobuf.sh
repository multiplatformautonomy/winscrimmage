#!/bin/bash
#NOTE: This script is no longer being used.

#This script builds and installs protobuf. However, this version of protobuf has an unusual
#error where the compiler falsely identifies certain functions as pure virtual. An identical issue can be found here: https://github.com/protocolbuffers/protobuf/issues/2642

#protobuf 3.12.3 is available through pacman and compiles without issues, but this version isn't
#officially supported by scrimmage.

#To use protobuf 3.3.0:
#	- comment line 42
#	- Uncomment lines 41, 47, 48, 52, 53, and 54
# 	- On line 43 of buildScrimmage.sh, prepend a '-' so it reads '-mingw-w64-x86_64-protobuf'

#To use protobuf 3.12.3:
#	- comment lines 41, 47, 48, 52, 53, and 54
# 	- Uncomment line 42
#	- On line 43 of buildScrimmage.sh, delete the preceding '-' so it reads 'mingw-w64-x86_64-protobuf'

#error catching
set -e
trap 'echo "buildProtobuf.sh failed!"' ERR

#checking to see if we are doing a debug build - buildScrimmage.sh should have handled any bad arguments.
if [[ $1 == "debug" ]]; then
	protobuf_cxx_flags="-O0 -g3 -ggdb -DDEBUG"
else
	protobuf_cxx_flags=""
fi

mkdir -p deps
cd deps

#downloading protoc source
if [ ! -d protobuf ]; then
	git clone https://github.com/google/protobuf.git
fi
	
cd protobuf
git checkout v3.3.0
#git checkout v3.12.3

#apply patch - patches 02 and 03 only need to be enabled for v3.3.0
third_party_dir="../../scrimmage/3rd-party"
$third_party_dir/setup/apply-patch.sh ../../patches/protobuf_01_prevent_setup_py_from_using_unavailable_compiler_flags.patch
$third_party_dir/setup/apply-patch.sh ../../patches/protobuf_02_prevent_invalid_conversion_errors_in_python_components.patch
$third_party_dir/setup/apply-patch.sh ../../patches/protobuf_03_poor_fix_for_GetMessage_pure_virtual_error.patch

#configue and build - if v3.12.3 is being used, we should download using pacman instead of building.
#If this doesn't work, this script will need to be run again.
./autogen.sh || ./autogen.sh
./configure CXXFLAGS="$protobuf_cxx_flags" --host="${MINGW_CHOST}" --prefix="/usr/local" && mingw32-make -j8
mingw32-make install

#build and install the python components
#TODO: it would be smart to eventually move most of the env usage into export statements in buildScrimmage.sh
cd python
env PYTHONHOME=/mingw64 python3 setup.py build --cpp_implementation
env PYTHONHOME=/mingw64 python3 setup.py install --cpp_implementation
