#!/bin/bash
#This script downloads and installs rapidxml.

#error catching - exit program if any command fails 
set -e
trap 'echo "getrapidxml.sh failed!"' ERR

#download and unzip rapidxml
mkdir -p deps
cd deps

if [ ! -d rapidxml-1.13 ]; then
	wget -L https://sourceforge.net/projects/rapidxml/files/rapidxml/rapidxml%201.13/rapidxml-1.13.zip
	unzip rapidxml-1.13.zip
	rm rapidxml-1.13.zip
fi

mkdir -p /usr/local/include
cp -r rapidxml-1.13 /usr/local/include/rapidxml
