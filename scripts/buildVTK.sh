#!/bin/bash
#The precompiled VTK is missing some modules, so we have to build with those modules enabled
#compiling with gcc is prohibitively slow, so I am using clang here. It hasn't caused any problems yet.

#error catching
set -e
trap 'echo "buildVTK failed!"' ERR

#checking to see if we are doing a debug build - buildScrimmage.sh should have handled any bad arguments.
if [[ $1 == "debug" ]]; then
	build_type="Debug"
else
	build_type="Release"
fi

#cloning the repo
mkdir -p deps
cd deps
if [ ! -d vtk ]; then 
	git clone --recursive https://gitlab.kitware.com/vtk/vtk.git
fi

cd vtk
git checkout v9.0.0

#apply patches
third_party_dir="../../scrimmage/3rd-party"
$third_party_dir/setup/apply-patch.sh ../../patches/vtk_01_prevent_using_strptime.patch
$third_party_dir/setup/apply-patch.sh ../../patches/vtk_02_fix_glew_undefined_memset.patch

#build - use Module_vtkIOGeoJSON=ON for older versions, and VTK_MODULE_ENABLE_IOGeoJSON for 9.0.0
mkdir -p build
cd build

#For older versions, '-fcommon' was used to work around a libtiff multiple definitions error
env CC=$(which clang) CXX=$(which clang++) cmake .. -G "MSYS Makefiles" \
                                                    -DCMAKE_CXX_FLAGS="-fuse-ld=lld -Wno-everything" \
						    -DVTK_MODULE_ENABLE_VTK_IOGeoJSON=YES \
						    -DCMAKE_BUILD_TYPE=$build_type \
                                                    -DCMAKE_INSTALL_PREFIX="/usr/local"

make -j8

#install
make install
