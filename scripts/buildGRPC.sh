#!/bin/bash
#This builds GRPC - The 'route_guide' example was compiled, and it runs in both the msys terminal 
#and windows cmd.

#error catching
set -e
trap 'echo "buildGRPC.sh failed!"' ERR

#checking to see if we are doing a debug build - buildScrimmage.sh should have handled any bad arguments.
if [[ $1 == "debug" ]]; then
	build_type="dbg"
else
	build_type="opt"
fi

#cloning and checking out the required commit
mkdir -p deps
cd deps

if [ ! -d grpc ]; then 
	git clone https://github.com/grpc/grpc
fi 

cd grpc
git checkout v1.2.1

#apply the three scrimmage patches as well as two winscrimmage patches.
third_party_dir="../../scrimmage/3rd-party"
$third_party_dir/setup/apply-patch.sh $third_party_dir/patches/grpc-0001-Include-lib-dir-in-link-flags.patch
$third_party_dir/setup/apply-patch.sh $third_party_dir/patches/grpc-0002-Fix-build-with-more-recent-openssl.patch
$third_party_dir/setup/apply-patch.sh $third_party_dir/patches/grpc-0001-Fix-incorrect-linker-commands-in-grpc.patch
$third_party_dir/setup/apply-patch.sh ../../patches/grpc_01_fix_windows_h_include_order.patch
$third_party_dir/setup/apply-patch.sh ../../patches/grpc_02_prevent_Wno_error_from_being_overriden.patch

#build and install
#This command uses the protobuf that was just built. It will work for the pacman protobuf too, but
#there is some stuff to remove if we end up sticking with the pacman version.
env PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/lib/pkgconfig \
    LDFLAGS="-L/usr/local/lib" \
    CPPFLAGS="-Wno-error -I/usr/local/include" make -j8 HAS_SYSTEM_PROTOBUF=true \
                                               PROTOC=$(which protoc) \
					       CONFIG=$build_type && make install prefix=/usr/local
