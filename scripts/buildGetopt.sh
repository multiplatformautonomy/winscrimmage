#!/bin/bash
#Windows doesn't have getopt, so we are using a port to avoid having to rewrite the arg parsing.
#MinGW has a getopt header, but nothing we can link to.

#error catching
set -e
trap 'echo "buildGetopt.sh failed!"' ERR

#downloading getopt if we haven't already
mkdir -p deps
cd deps

if [ ! -d getopt ]; then
	wget -O getopt.zip https://gist.github.com/superwills/5815344/archive/607ee30b1551e10691e2b04ff6cafb2b8b107612.zip
      	unzip getopt.zip
	rm getopt.zip
	mv 5815344* getopt	
fi

cd getopt

#build and install
mkdir -p /usr/local/lib
if [ ! -f /usr/local/lib/libgetopt.dll ]; then 
	gcc -shared -o libgetopt.dll getopt.c
	cp libgetopt.dll /usr/local/lib/
fi

#cmake doesn't look for getopt, so putting it in gcc's path makes things easier
#msys can't actually make symlinks, unfortunately...
if [ ! -f /mingw64/lib/libgetopt.dll ]; then
	ln -s /usr/local/lib/libgetopt.dll /mingw64/lib/libgetopt.dll
fi
