# winSCRIMMAGE
This is an attempt to build scrimmage natively on windows. It uses msys2 as a build environment so that utilities like MinGW, bash, and the package manager 'pacman' can be leveraged. 

Usage:
1. install msys2
2. run [msys2 install directory]/mingw64.exe
3. in the msys2 terminal, run the following commands:

<pre>
$ pacman -S --noconfirm git
$ git clone https://gitlab.com/multiplatformautonomy/winscrimmage.git
$ cd winscrimmage
$ ./buildScrimmage.sh
</pre>

For debug builds of scrimmage and its dependencies, run:
<pre>
$ ./buildScrimmage.sh debug
</pre>

Notes:
- Scrimmage and its dependencies had to be modified for compatibility with windows and to work around certain errors.
	- These changes are applied as git patches.
	- See patches/README.md for an explanation of each patch.
- This is a WIP. It can build and run scrimmage with VTK, but there may still be issues.
- We will have to decide between using the offially supported version of protobuf and using a sketchy workaround, or a newer version that isn't officially supported. See debug/README.md for more info.
- A newer version of VTK is being used (9.0.0). This is a significant version bump, but earlier versions were failing to compile with difficult to pin down errors.
