# Patches
This folder contains the patches used to modify scrimmage before building. For the
most part, the scrimmage patches modify cmake files to address linking issues that
arise. Some patches also add windows-style signal handlers or prevent scrimmage from
using functions that aren't provided by the MinGW headers.

Some of the later patches modify the plugin manager to allow it to use windows paths
and search for dlls. The buildScrimmage.sh script adds environment variables that work
with the plugin manager in MSYS, but these environment variables don't seem to work with Git
Bash.

**SCRIMMAGE Patches**
- scrimmage_01 - makes sure scrimmage-msgs is linked against scrimmage-protos.
- scrimmage_02 - provides a Windows signal handler for SimUtils.cpp.
- scrimmage_03 - adds dl to CMAKE_DL_LIBS in CMakeLists.txt.
- scrimmage_04 - allows main.cpp to use getopt and adds a signal handler.
	- links against getopt in share/scrimmage/CMakeLists.txt.
	- includes getopt.h in share/scrimmage/main.cpp.
	- provides a windows signal handler to share/scrimmage/main.cpp.
- scrimmage_05 - switches the order of boost libraries in CMakeLists.txt so static linking doesn't error. 
	- This isn't necessary in most cases, but addresses an issue that may be difficult to pin down.
	- The linking issue only occurs if cmake can't find shared libraries for boost.
- scrimmage_06 - addresses a couple plugin issues. 
	- links against Winsock in src/plugins/autonomy/ArduPilot/CMakeLists.txt.
	- prevents joystick plugins from being built. 
- scrimmage_07 - adds the -DBUILDING_DLL flag to compiler options in src/plugins/autonomy/ScrimmageOpenAIAutonomy/CmakeLists.txt.
	- This flag is used by include/scrimmage/common/Visibility.h to specify DLL_PUBLIC.
	- DLL_PUBLIC is used by ScrimmageOpenAIAutonomy to add 'dllexport' to classes and functions.
- scrimmage_08 - replaces MIN and MAX with fmin and fmax in src/plugins/sensor/LOSSensor/LOSSensor.cpp.
	- These macros don't seem to to be defined in MSYS.
	- Using fmin and fmax also prevents double execution.
- scrimmage_09 - links against Winsock in src/plugins/motion/JSBSimModel/CMakeLists.txt.
- scrimmage_10 - provides a Windows signal handler to python/scrimmage/bindings_src/src/py_openai_env.cpp.
- scrimmage_11 - adds the full dll suffix on Windows to python/scrimmage/bindings_src/CMakeLists.txt.
- scrimmage_12 - adds the appropriate path seperator to src/common/FileSearch.cpp.
	- This works for MSYS and cmd, but doesn't seem to work for git bash. 
- scrimmage_13 - modified include/scrimmage/plugin_manager/PluginManager.h to look for dlls.
- scrimmage_14 - fixed some include and linking issues caused by using VTK 9.0.0.
	- In this newer version, vtkTexture.h and vtkUnsignedCharArray.h have to be included. 
	- The target must also be linked against vtkCommonComputationalGeometry and vtkRenderingFreeType.
- scrimmage_15 - adds windows getopt support and a windows signal handler to scrimmage-playback.
	- links against getopt in share/scrimmage-playback/CMakeLists.txt.
	- includes getopt.h in share/scrimmage-playback/main.cpp.
	- adds the windows signal handler to share/scrimmage-playback/main.cpp.
- scrimmage_16 - adds vtk_module_autoinit calls to any CMakeList that links against VTK_LIBRARIES.
	- This step was handled automatically in VTK versions prior to 9.0.0.
	- Related: https://discourse.vtk.org/t/two-questions-about-new-module-system/2864
- scrimmage_17 - fixes a log parsing issue caused by new-line conversion while using 'open' on windows.
- scrimmage_18 - fixes some compatibility issues in scripts/plot_3d_fr.py due to matplotlib version differences.
	- A newer version is being used, since the older versions seem to have trouble finding some dependencies.
- scrimmage_19 - prevents the SimpleCapture plugin from rechecking the distance.
	- The enemy is added to the capture list before the distance is checked.
	- If the enemy has moved out of capture distance between the Predator plugin's distance check and the SimpleCapture check, the enemy will be added to the capture list without being removed from the field.
	- This causes the predator to spiral around that enemy until the mission ends.

**GRPC Patches**
- grpc_01 - moves the windows.h include so that \_WIN32_WINNT isn't used before it is defined in include/grpc/impl/codegen/port_platform.h.
- grpc_02 - prevents the -Wno-error flag from being overridden in Makefile.

**JSBSim Patches**
- jsbsim_01 - prevents jsbsim from using feenableexcept in src/JSBSim.cpp.
	- this function isn't available from MinGW, so Windows functions should be used instead.

**VTK Patches**
- vtk_01 - prevents VTK from using strptime in Filters/General/vtkDateToNumeric.cxx.
	- The build uses gcc, but this function isn't available from MinGW.
- vtk_02 - links against ucrtbase in ThirdParty/glew/vtkglew/CMakeLists.txt.
	- This fixes some undefined references to memest in glew.

**Protobuf Patches**
- protobuf_01 - prevents setup.py from using unavailable compiler flags.
	- Protobuf is available through pacman, but the same version isn't available through pip.
	- As such, we have to build the python components of protobuf.
	- This patch prevents setup.py from using /MT, which isn't available in gcc.
- protobuf_02 - fixes an invalid conversion error that occurs with protobuf 3.3.0.
	- adds (char\*) in a couple places.
- protobuf_03 - a sketchy fix for an error involving GetMessage being identified as pure virtual.
	- Only occurs with protobuf 3.3.0.
	- This issue causes the scrimmage build to fail because the compiler thinks GeneratedMessageReflection is an abstract class.
	- If GetMessage isn't actually being overriden for some reason, this 'fix' just defers the issue to runtime.
	- As such, it should be verified that GetMessage is actually overriden using gdb.
