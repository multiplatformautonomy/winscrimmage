#!/bin/bash
#Running this script in a MSYS MinGW64 terminal will install all required build tools, build
#scrimmage dependencies, and build scrimmage. It will also try to replicate the environment setup
#that a native scrimmage build will perform.
#Running ./buildScrimmage.sh debug will build with debug symbols. This is useful for debugging with 
#gdb.

#error catching
set -e
trap 'echo "buildScrimmage.sh failed!"' ERR

#Build info
scrimmage_cxx_flags=""
build_type="Release"

#checking to see if we are doing a debug build
if [[ $1 != "" ]]; then
	if [[ $1 == "debug" ]]; then
		echo "building in debug mode! This means dependencies will be built with debug symbols."
	scrimmage_cxx_flags="-g3 -O0 -ggdb"
	build_type="Debug"
	else
		echo "$1 is not a valid option!"
		exit
	fi
fi

#Install required tools if we don't already have them
#The tools starting with '-' are marked to be skipped - this allows us to exclude certain dependencies
#and build tools without losing track of them.
buildTools="mingw-w64-x86_64-gcc
	    mingw-w64-x86_64-clang
	    mingw-w64-x86_64-lld
	    mingw-w64-x86_64-make
	    mingw-w64-x86_64-cmake
	    mingw-w64-x86_64-gdb
	    mingw-w64-x86_64-libtool
	    mingw-w64-x86_64-gflags
	    mingw-w64-x86_64-go
	    mingw-w64-x86_64-pybind11
	    mingw-w64-x86_64-dlfcn
	    mingw-w64-x86_64-eigen3
	    -mingw-w64-x86_64-protobuf
	    mingw-w64-x86_64-python-setuptools
	    mingw-w64-x86_64-python-numpy
	    mingw-w64-x86_64-python-matplotlib
	    mingw-w64-x86_64-python-pandas
	    make
	    autoconf
	    automake-wrapper
	    unzip
	    tar
	    vim
	    winpty"

echo "looking for required build tools..."
for tool in $buildTools; do
	#skip selected tools
	if [[ $tool == -* ]]; then 
		echo "tool skipped..."
		continue
	fi

	#"pacman -Qi [packagename]" returns 1 if the package is found
	if ! (pacman -Qi $tool &> /dev/null) ; then
		pacman -S --noconfirm $tool
	fi
done

#Download SCRIMMAGE source
if [ ! -d scrimmage ]; then 
	git clone https://github.com/gtri/scrimmage.git
fi

#Run build / get scripts for each dependency - putting each dependency in its own script makes it
#easier to exclude certain dependencies from being built.
./scripts/getrapidxml.sh $1
./scripts/buildBoost.sh $1
./scripts/buildGeographicLib.sh $1
./scripts/buildJSBSim.sh $1
./scripts/buildProtobuf.sh $1
./scripts/buildGRPC.sh $1
./scripts/buildVTK.sh $1
./scripts/buildGetopt.sh $1

#apply scrimmage patches - see patches/README.md for an explanation of what each patch does.
cd scrimmage
scrimmage_patches=$(find ../patches -regex .*/scrimmage.*)
for patch in $scrimmage_patches; do
	printf "\napplying patch:\t$patch\n"
	./3rd-party/setup/apply-patch.sh $patch
done

#build SCRIMMAGE
if [ -d build ]; then
	rm -r build
fi 

mkdir build
cd build

#building
cmake .. -G "MSYS Makefiles" \
         -DPYTHON_MIN_VERSION=3.6 \
		 -DCMAKE_BUILD_TYPE=$build_type \
		 -DCMAKE_CXX_FLAGS="$scrimmage_cxx_flags -I/usr/local/include/boost-1_58 -I/usr/local/include" \
		 -DPYTHON_SUFFIX=$(python3-config --extension-suffix)

make -j8

#TODO: still need to make sure this works on a fresh install
pushd ../python
env PYTHONHOME=/mingw64 python3 setup.py develop
popd

#Note: The remainder of this script tries to sort out some path issues that may arise.
#      Since we don't use the ppa to download the third party dependencies, '/opt/scrimmage/*/setup.sh
#      is missing. As such, we have to add the environment variable JSBSIM_ROOT and append some
#      additional directories to the path.

#adding mingw-compiled libraries to PATH in .bashrc
if ! grep /mingw64/lib:/usr/local/lib ~/.bashrc &>/dev/null ; then 
	echo 'export PATH=$PATH:/mingw64/lib:/usr/local/lib' >> ~/.bashrc
fi

#SCRIMMAGE should be linking against the msys 'system' python now.
if ! grep PYTHONHOME ~/.bashrc &>/dev/null ; then
	echo "export PYTHONHOME=/mingw64" >> ~/.bashrc
fi

#Previously, we were reading the python location from CMakeCache.txt, since we were using the host
#machine's python. If we can install all the python dependencies for the msys python, this won't be
#necessary. However, it may be a better idea to use the host's python anyway.
#if ! grep -E python[0-9]{2} ~/.bashrc &>/dev/null ; then
#	pyPath=$(grep PYTHON_EXECUTABLE:FILEPATH CMakeCache.txt)
#	pyPath=/c$(echo $pyPath | sed 's/PYTHON_EXECUTABLE:FILEPATH=C:\(.*\)\/python\.exe/\1/')
#	echo 'export PATH=$PATH:'$pyPath >> ~/.bashrc
#fi

#adding JSBSIM_ROOT to .bashrc
if ! grep JSBSIM_ROOT ~/.bashrc &>/dev/null ; then
	echo "export JSBSIM_ROOT=$HOME/winscrimmage/deps/JSBSim" >> ~/.bashrc
fi

#fixing setenv paths so that paths can be read by MSYS - unfortunately this doesn't seem compatible
#with git bash.
sed -i 's/C:/\/C/g' ~/.scrimmage/env/scrimmage-setenv
sed -i 's/plugin_libs/bin/g' ~/.scrimmage/env/scrimmage-setenv

#sourcing ~/.scrimmage/setup.bash in .bashrc
if ! grep $HOME/.scrimmage/setup.bash ~/.bashrc &>/dev/null ; then
	echo "source $HOME/.scrimmage/setup.bash" >> ~/.bashrc
fi
